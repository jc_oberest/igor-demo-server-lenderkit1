# Docker PHP container ( [Work In Progress]() )

All php-relative scripts (such as composer or artisan) should be run inside the `app` docker container.
To open docker container bash you can run (on **root level**)

`make php-bash`

`/src` folder contains its own Makefile (**application level**), which simplify some standard commands inside docker php container.
After you opened `php-bash` you can run `make info` to get information of available commands.

### Composer

All composer commands should be executed with composer limit option. For example:

`COMPOSER_MEMORY_LIMIT=2G composer install`

To simplify composer usage you can run composer from a make utility like this:

```bash
make composer-install   #or make ci
make composer-update    #or make cu
make composer-autoload  #or make ca
```

### Artisan

By default `php artisan` command will be launched as API application. So admin commands won't be available here 
(as well as admin vendor:publish options). To run admin artisan simply run `php admin/artisan`.

Furthermore, application have 2 different bootstrap folders and 2 different bootstrap caches. So if you want to clean
your cache you need to run 2 commands, one for API and second for the Admin.

To simplify common used commands you can run:

```bash
make cache-clear
make cache
make swagger
``` 
